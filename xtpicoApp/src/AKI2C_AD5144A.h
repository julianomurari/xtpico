/*
 * AKI2C_AD5144A.h
 *
 *  Created on: February 7, 2022
 *      Author: krisztianloki
 */

#ifndef _I2C_AD5144A_H_
#define _I2C_AD5144A_H_

#include "AKI2C.h"

/* Three different nominal resistances */
#define AKI2C_AD5144A_MAXRES_10k				0
#define AKI2C_AD5144A_MAXRES_100k				1

/* AD5144A datasheet Table 14 */
#define AKI2C_AD5144A_WR_RDAC_CMD				0x1000
#define AKI2C_AD5144A_RD_CMD					0x3000

#define AKI2C_AD5144A_RD_EEPROM					0x01
#define AKI2C_AD5144A_RD_RDAC					0x03

/* AD5144A datasheet Table 15 */
#define AKI2C_AD5144A_ADDR_RDAC1				0x0000
#define AKI2C_AD5144A_ADDR_RDAC2				0x0100
#define AKI2C_AD5144A_ADDR_RDAC3				0x0200
#define AKI2C_AD5144A_ADDR_RDAC4				0x0300

#define AKI2C_AD5144A_RDAC1_ValueString				"AKI2C_AD5144A_RDAC1_VALUE"
#define AKI2C_AD5144A_RDAC1_ReadString				"AKI2C_AD5144A_RDAC1_READ"
#define AKI2C_AD5144A_RDAC2_ValueString				"AKI2C_AD5144A_RDAC2_VALUE"
#define AKI2C_AD5144A_RDAC2_ReadString				"AKI2C_AD5144A_RDAC2_READ"
#define AKI2C_AD5144A_RDAC3_ValueString				"AKI2C_AD5144A_RDAC3_VALUE"
#define AKI2C_AD5144A_RDAC3_ReadString				"AKI2C_AD5144A_RDAC3_READ"
#define AKI2C_AD5144A_RDAC4_ValueString				"AKI2C_AD5144A_RDAC4_VALUE"
#define AKI2C_AD5144A_RDAC4_ReadString				"AKI2C_AD5144A_RDAC4_READ"

#define AKI2C_AD5144A_MaxResString				"AKI2C_AD5144A_MAXRES"

class RDAC {
public:
	RDAC(asynPortDriver *driver, int address, const char *readString, const char *valueString);

	int readParam;
	int valueParam;
	int address;
	int index;
};

/*
 * Chip			: Analog AD5144A
 * Function		: nonvolatile digital potentiometer
 * Bus			: I2C
 * Access		: TCP/IP socket on AK-NORD XT-PICO-SX
 */
class AKI2C_AD5144A: public AKI2C {
public:
	AKI2C_AD5144A(const char *portName, const char *ipPort,
			int devCount, const char *devInfos, int priority, int stackSize);
	virtual ~AKI2C_AD5144A();

	/* These are the methods that we override from AKI2C */
	virtual asynStatus writeInt32(asynUser *pasynUser, epicsInt32 value);
	virtual asynStatus writeFloat64(asynUser *pasynUser, epicsFloat64 value);
	void report(FILE *fp, int details);
	/* These are new methods */

protected:
	/* Our parameter list */
	RDAC* rdacs[4];
	int FIRST_AKI2C_AD5144A_PARAM;
	int AKI2C_AD5144A_MaxRes;

private:
	const RDAC* readParamToRdac(int readParam);
	const RDAC* valueParamToRdac(int valueParam);
	asynStatus write(int addr, unsigned short cmd, unsigned short len);
	asynStatus read(int addr, unsigned short cmd, unsigned short *val, unsigned short len);
	asynStatus readValue(int addr, const RDAC *rdac);
	asynStatus writeValue(int addr, const RDAC *rdac, double val);

	double mMaxRes;
};

#endif /* _I2C_AD5144A_H_ */
