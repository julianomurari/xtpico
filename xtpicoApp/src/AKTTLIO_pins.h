/*
 * AKTTLIO_pins.h
 *
 *  Created on: Feb 26, 2016
 *      Author: hinkokocevar
 */

#ifndef _AKTTLIO_PINS_H_
#define _AKTTLIO_PINS_H_


#include "AKTTLIO.h"

#define AKTTLIO_MAX_PINS						8

#define AKTTLIO_READ_PINS_FUNC					0x00
#define AKTTLIO_CLEAR_PINS_FUNC					0x01
#define AKTTLIO_SET_PINS_FUNC					0x02
#define AKTTLIO_INPUT_PINS_FUNC					0x03
#define AKTTLIO_OUTPUT_PINS_FUNC				0x04
#define AKTTLIO_NOPULLUP_PINS_FUNC				0x05
#define AKTTLIO_PULLUP_PINS_FUNC				0x06

#define AKTTLIO_ReadString					"AKTTLIO_READ"
#define AKTTLIO_LevelPin0String				"AKTTLIO_LEVEL_PIN0"
#define AKTTLIO_LevelPin1String				"AKTTLIO_LEVEL_PIN1"
#define AKTTLIO_LevelPin2String				"AKTTLIO_LEVEL_PIN2"
#define AKTTLIO_LevelPin3String				"AKTTLIO_LEVEL_PIN3"
#define AKTTLIO_LevelPin4String				"AKTTLIO_LEVEL_PIN4"
#define AKTTLIO_LevelPin5String				"AKTTLIO_LEVEL_PIN5"
#define AKTTLIO_LevelPin6String				"AKTTLIO_LEVEL_PIN6"
#define AKTTLIO_LevelPin7String				"AKTTLIO_LEVEL_PIN7"
#define AKTTLIO_DirPin0String				"AKTTLIO_DIR_PIN0"
#define AKTTLIO_DirPin1String				"AKTTLIO_DIR_PIN1"
#define AKTTLIO_DirPin2String				"AKTTLIO_DIR_PIN2"
#define AKTTLIO_DirPin3String				"AKTTLIO_DIR_PIN3"
#define AKTTLIO_DirPin4String				"AKTTLIO_DIR_PIN4"
#define AKTTLIO_DirPin5String				"AKTTLIO_DIR_PIN5"
#define AKTTLIO_DirPin6String				"AKTTLIO_DIR_PIN6"
#define AKTTLIO_DirPin7String				"AKTTLIO_DIR_PIN7"
#define AKTTLIO_PullUpPin0String			"AKTTLIO_PULLUP_PIN0"
#define AKTTLIO_PullUpPin1String			"AKTTLIO_PULLUP_PIN1"
#define AKTTLIO_PullUpPin2String			"AKTTLIO_PULLUP_PIN2"
#define AKTTLIO_PullUpPin3String			"AKTTLIO_PULLUP_PIN3"
#define AKTTLIO_PullUpPin4String			"AKTTLIO_PULLUP_PIN4"
#define AKTTLIO_PullUpPin5String			"AKTTLIO_PULLUP_PIN5"
#define AKTTLIO_PullUpPin6String			"AKTTLIO_PULLUP_PIN6"
#define AKTTLIO_PullUpPin7String			"AKTTLIO_PULLUP_PIN7"

/** Driver for AK-NORD XT-PICO-SXL TTL IO port access over TCP/IP socket */
class AKTTLIO_pins: public AKTTLIO {
public:
	AKTTLIO_pins(const char *portName, const char *ipPort,
			int priority, int stackSize);
	virtual ~AKTTLIO_pins();

	/* These are the methods that we override from AKTTLIO */
	virtual asynStatus writeInt32(asynUser *pasynUser, epicsInt32 value);
	void report(FILE *fp, int details);
	/* These are new methods */

protected:
	/* Our parameter list */
	int AKTTLIO_Read;
#define FIRST_AKTTLIO_PARAM AKTTLIO_Read
	int AKTTLIO_LevelPin0;
	int AKTTLIO_LevelPin1;
	int AKTTLIO_LevelPin2;
	int AKTTLIO_LevelPin3;
	int AKTTLIO_LevelPin4;
	int AKTTLIO_LevelPin5;
	int AKTTLIO_LevelPin6;
	int AKTTLIO_LevelPin7;
	int AKTTLIO_DirPin0;
	int AKTTLIO_DirPin1;
	int AKTTLIO_DirPin2;
	int AKTTLIO_DirPin3;
	int AKTTLIO_DirPin4;
	int AKTTLIO_DirPin5;
	int AKTTLIO_DirPin6;
	int AKTTLIO_DirPin7;
	int AKTTLIO_PullUpPin0;
	int AKTTLIO_PullUpPin1;
	int AKTTLIO_PullUpPin2;
	int AKTTLIO_PullUpPin3;
	int AKTTLIO_PullUpPin4;
	int AKTTLIO_PullUpPin5;
	int AKTTLIO_PullUpPin6;
	int AKTTLIO_PullUpPin7;

private:
	asynStatus read(int addr, unsigned char func, unsigned char *val);
	asynStatus write(int addr, unsigned char func, unsigned char val);
	asynStatus writeLevel(int addr, unsigned char param, unsigned char val);
	asynStatus readLevel(int addr, unsigned char param);
	asynStatus readLevelAll(int addr);
	asynStatus writeDirection(int addr, unsigned char param, unsigned char val);
	asynStatus writePullUps(int addr, unsigned char param, unsigned char val);
};

#endif /* _AKTTLIO_PINS_H_ */
